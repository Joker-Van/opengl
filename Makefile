CC=g++
COMPFLAGS=./lib/LoadShaders.cpp -lGL -lGLEW -lglfw -I ./include

all:triangles glversion cachedata subroutine drawcommand restart
	
glversion:
	${CC} -o ./pgms/glversion.out ./src/glversion/glversion.cpp ${COMPFLAGS}

triangles:
	${CC} -o ./pgms/gltriangle.out ./src/1.1triangle/triangles.cpp ${COMPFLAGS} 

subroutine:
	${CC} -o ./pgms/glsubroutine.out ./src/2.6subroutine/subroutine.cpp ${COMPFLAGS}

cachedata:
	${CC} -o ./pgms/glcachedata.out ./src/3.2cachedata/cachedata.cpp ${COMPFLAGS}

drawcommand:
	${CC} -o ./pgms/gldrawcommand.out ./src/3.5drawcommand/drawcommands.cpp ${COMPFLAGS}

restart:
	${CC} -o ./pgms/glrestart.out ./src/3.6restart/restart.cpp ${COMPFLAGS}
