Jokervan opengl learning

---------------------------------------------------------------------------------------------

build environment
gcc:build-essential
glfw:api aboult GUI
glew:opengl advanced extension funcion
glm:math lib about vector and matrix
assimp:load a 3D model
blender

sudo apt install build-essential
sudo apt install libglfw3 libglfw3-dev
sudo apt install libglew2.1 libglew-dev
sudo apt install libglm-dev
sudo apt install assimp-utils libassimp5 libassimp-dev
sudo apt install blender

---------------------------------------------------------------------------------------------

get the infomation about opengl
glxinfo | grep "OpenGL version"

---------------------------------------------------------------------------------------------

content of src

1.1triangle:
	build two triangle

2.6subroutine:
	use subroutine to bring about the choice of triangle color
	fragment

3.2cachedata:
	test glCopyNamedBufferSubData and glNamedBufferSubData

3.5drawcommand:
	four different drawing methods

3.6restart:
	use primitive-restart to draw two triangle strips

glversion:
	get the version of the opengl

