#version 450 core

out vec4 fColor;

subroutine vec4 color();
subroutine uniform color changecolor;

subroutine (color) vec4 blue(){
    return vec4(0.0, 0.0, 1.0, 1.0);
}

subroutine (color) vec4 red(){
    return vec4(1.0, 0.0, 0.0, 1.0);
}

void main()
{
    fColor = changecolor();
}
