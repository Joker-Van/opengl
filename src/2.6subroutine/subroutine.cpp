//////////////////////////////////////////////////////////////////////////////
//
//  Triangles.cpp
//
//////////////////////////////////////////////////////////////////////////////
#include "stdio.h"
#include "vgl.h"
#include "LoadShaders.h"

enum VAO_IDs { Triangles, NumVAOs };
enum Buffer_IDs { ArrayBuffer, NumBuffers };
enum Attrib_IDs { vPosition = 0 };

GLuint  VAOs[NumVAOs];
GLuint  Buffers[NumBuffers];

const GLuint  NumVertices = 6;

//----------------------------------------------------------------------------
//
// init
//

void
init( void )
{
    glGenVertexArrays( NumVAOs, VAOs );
    glBindVertexArray( VAOs[Triangles] );

    GLfloat  vertices[NumVertices][2] = {
        { -0.90f, -0.90f }, {  0.85f, -0.90f }, { -0.90f,  0.85f },  // Triangle 1
        {  0.90f, -0.85f }, {  0.90f,  0.90f }, { -0.85f,  0.90f }   // Triangle 2
    };

    glCreateBuffers( NumBuffers, Buffers );
    glBindBuffer( GL_ARRAY_BUFFER, Buffers[ArrayBuffer] );
    glBufferStorage( GL_ARRAY_BUFFER, sizeof(vertices), vertices, 0);

    ShaderInfo  shaders[] =
    {
        { GL_VERTEX_SHADER, "../src/2.6subroutine/triangles.vert" },
        { GL_FRAGMENT_SHADER, "../src/2.6subroutine/triangles.frag" },
        { GL_NONE, NULL }
    };

    GLuint program = LoadShaders( shaders );
    glUseProgram( program );

#if 1
    GLint changecolorLoc = glGetSubroutineUniformLocation(program, GL_FRAGMENT_SHADER, "changecolor");
    
    if (changecolorLoc < 0) {
        printf("changecolorLoc error \n");
    }

    GLuint blueIndex = glGetSubroutineIndex(program, GL_FRAGMENT_SHADER, "blue");
    GLuint redIndex = glGetSubroutineIndex(program, GL_FRAGMENT_SHADER, "red");
    
    if (redIndex == GL_INVALID_INDEX || blueIndex == GL_INVALID_INDEX) {
	printf("Index error\n");
    }else {
	GLsizei n;
	//glGetIntegerv(GL_MAX_SUBROUTINE_UNIFORM_LOCATIONS, &n);
	glGetProgramStageiv(program, GL_FRAGMENT_SHADER, GL_ACTIVE_SUBROUTINE_UNIFORM_LOCATIONS, &n);
	
	printf("n indices      :%d\n",n);
	printf("changecolorLoc :%d\n",changecolorLoc);
	printf("blueIndex      :%d\n",blueIndex);
	printf("redIndex       :%d\n",redIndex);

	GLuint *indices = new GLuint[n];
	indices[changecolorLoc] = redIndex;

	glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, n, indices);

	delete [] indices;
    }    
#else
    GLuint blueIndex = glGetSubroutineIndex(program, GL_FRAGMENT_SHADER, "blue");
    GLuint redIndex = glGetSubroutineIndex(program, GL_FRAGMENT_SHADER, "red");

    glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &redIndex);
#endif

    glVertexAttribPointer( vPosition, 2, GL_FLOAT,
                           GL_FALSE, 0, BUFFER_OFFSET(0) );
    glEnableVertexAttribArray( vPosition );
}

//----------------------------------------------------------------------------
//
// display
//

void
display( void )
{
    static const float black[] = { 0.0f, 0.0f, 0.0f, 0.0f };

    glClearBufferfv(GL_COLOR, 0, black);

    glBindVertexArray( VAOs[Triangles] );
    glDrawArrays( GL_TRIANGLES, 0, NumVertices );
}

//----------------------------------------------------------------------------
//
// main
//

#ifdef _WIN32
int CALLBACK WinMain(
  _In_ HINSTANCE hInstance,
  _In_ HINSTANCE hPrevInstance,
  _In_ LPSTR     lpCmdLine,
  _In_ int       nCmdShow
)
#else
int
main( int argc, char** argv )
#endif
{
    glfwInit();

    GLFWwindow* window = glfwCreateWindow(800, 600, "Triangles", NULL, NULL);

    glfwMakeContextCurrent(window);
    glewInit();

    init();

    while (!glfwWindowShouldClose(window))
    {
        display();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
}
