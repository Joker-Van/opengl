//////////////////////////////////////////////////////////////////////////////
//
//  Triangles.cpp
//
//////////////////////////////////////////////////////////////////////////////

#include "vgl.h"
#include "LoadShaders.h"

enum VAO_IDs { Triangles, NumVAOs };
enum Buffer_IDs { ArrayBuffer, NumBuffers };
enum Attrib_IDs { vPosition = 0 };

GLuint  VAOs[NumVAOs];
GLuint  Buffers[NumBuffers];

const GLuint  NumVertices = 6;

//----------------------------------------------------------------------------
//
// init
//

void
init( void )
{
    glGenVertexArrays( NumVAOs, VAOs );
    glBindVertexArray( VAOs[Triangles] );

    GLfloat  vertices[NumVertices][2] = {
        { -0.90f, -0.90f }, {  0.85f, -0.90f }, { -0.90f,  0.85f },  // Triangle 1
        {  0.90f, -0.85f }, {  0.90f,  0.90f }, { -0.85f,  0.90f }   // Triangle 2
    };

    GLfloat subvertices1[3][2] = {
    	{ -0.90f, 0.90f }, { 0.85f, 0.90f }, { -0.90f, -0.85f }
    };

    GLfloat subvertices2[3][2] = {
    	{ 0.90f, -0.90f }, { -0.85f, -0.90f }, { 0.90f, 0.85f }
    };

#if 0
    glCreateBuffers( NumBuffers, Buffers );
    glBindBuffer( GL_ARRAY_BUFFER, Buffers[ArrayBuffer] );
    //GL_DYNAMIC_STORAGE_BIT
    glBufferStorage( GL_ARRAY_BUFFER, sizeof(vertices), NULL, GL_DYNAMIC_STORAGE_BIT);
    glNamedBufferSubData(Buffers[ArrayBuffer], 0, sizeof(subvertices1), subvertices1);
    glNamedBufferSubData(Buffers[ArrayBuffer], sizeof(subvertices1), sizeof(subvertices2), subvertices2);
#else
    GLuint buffer1, buffer2, sumbuffer;
    glCreateBuffers(1, &sumbuffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, sumbuffer);
    glBufferStorage(GL_COPY_WRITE_BUFFER, sizeof(vertices), NULL, GL_DYNAMIC_STORAGE_BIT);

    glCreateBuffers(1, &buffer1);
    glBindBuffer(GL_COPY_READ_BUFFER, buffer1);
    glBufferStorage(GL_COPY_READ_BUFFER, sizeof(subvertices1), subvertices1, 0);
    glCopyNamedBufferSubData(buffer1, sumbuffer, 0, 0, sizeof(subvertices1));

    glCreateBuffers(1, &buffer2);
    glBindBuffer(GL_COPY_READ_BUFFER, buffer2);
    glBufferStorage(GL_COPY_READ_BUFFER, sizeof(subvertices2), subvertices2, 0);
    glCopyNamedBufferSubData(buffer2, sumbuffer, 0, sizeof(subvertices1), sizeof(subvertices2));
    
    glBindBuffer(GL_ARRAY_BUFFER, sumbuffer);
#endif

    ShaderInfo  shaders[] =
    {
        { GL_VERTEX_SHADER, "../src/3.2cachedata/triangles.vert" },
        { GL_FRAGMENT_SHADER, "../src/3.2cachedata/triangles.frag" },
        { GL_NONE, NULL }
    };

    GLuint program = LoadShaders( shaders );
    glUseProgram( program );

    glVertexAttribPointer( vPosition, 2, GL_FLOAT,
                           GL_FALSE, 0, BUFFER_OFFSET(0) );
    glEnableVertexAttribArray( vPosition );
}

//----------------------------------------------------------------------------
//
// display
//

void
display( void )
{
    static const float black[] = { 0.0f, 0.0f, 0.0f, 0.0f };

    glClearBufferfv(GL_COLOR, 0, black);

    glBindVertexArray( VAOs[Triangles] );
    glDrawArrays( GL_TRIANGLES, 0, NumVertices );
}

//----------------------------------------------------------------------------
//
// main
//

#ifdef _WIN32
int CALLBACK WinMain(
  _In_ HINSTANCE hInstance,
  _In_ HINSTANCE hPrevInstance,
  _In_ LPSTR     lpCmdLine,
  _In_ int       nCmdShow
)
#else
int
main( int argc, char** argv )
#endif
{
    glfwInit();

    GLFWwindow* window = glfwCreateWindow(800, 600, "Triangles", NULL, NULL);

    glfwMakeContextCurrent(window);
    glewInit();

    init();

    while (!glfwWindowShouldClose(window))
    {
        display();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
}
