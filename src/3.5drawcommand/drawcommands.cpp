//////////////////////////////////////////////////////////////////////////////
//
//  Triangles.cpp
//
//////////////////////////////////////////////////////////////////////////////

#include "vgl.h"
#include "LoadShaders.h"

enum EBO_IDs { Triangle, NumEBOs };
enum VAO_IDs { Triangles, NumVAOs };
enum Buffer_IDs { ArrayBuffer, NumBuffers };
enum Attrib_IDs { vPosition = 0 };

GLuint  EBOs[NumEBOs];
GLuint  VAOs[NumVAOs];
GLuint  Buffers[NumBuffers];

const GLuint  NumVertices = 6;

//----------------------------------------------------------------------------
//
// init
//

void
init( void )
{
    glGenVertexArrays( NumVAOs, VAOs );
    glBindVertexArray( VAOs[Triangles] );

    GLfloat  vertices[NumVertices][2] = {
        { -0.90f, -0.90f }, {  0.85f, -0.90f }, { -0.90f,  0.85f },  // Triangle 1
        {  0.90f, -0.85f }, {  0.90f,  0.90f }, { -0.85f,  0.90f }   // Triangle 2
    };

    GLfloat subvertices1[3][2] = {
    	{ -0.90f, 0.90f }, { 0.85f, 0.90f }, { -0.90f, -0.85f }
    };

    GLfloat subvertices2[3][2] = {
    	{ 0.90f, -0.90f }, { -0.85f, -0.90f }, { 0.90f, 0.85f }
    };

    glCreateBuffers( NumBuffers, Buffers );
    glBindBuffer( GL_ARRAY_BUFFER, Buffers[ArrayBuffer] );
    //GL_DYNAMIC_STORAGE_BIT
    glBufferStorage( GL_ARRAY_BUFFER, sizeof(vertices), NULL, GL_DYNAMIC_STORAGE_BIT);
    glNamedBufferSubData(Buffers[ArrayBuffer], 0, sizeof(subvertices1), subvertices1);
    glNamedBufferSubData(Buffers[ArrayBuffer], sizeof(subvertices1), sizeof(subvertices2), subvertices2);

    static const GLushort vertex_indices[] = {
        0, 1, 2 
    };

    glGenBuffers(NumEBOs, EBOs);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBOs[Triangle]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(vertex_indices), vertex_indices, GL_STATIC_DRAW);

    ShaderInfo  shaders[] =
    {
        { GL_VERTEX_SHADER, "../src/3.5drawcommand/triangles.vert" },
        { GL_FRAGMENT_SHADER, "../src/3.5drawcommand/triangles.frag" },
        { GL_NONE, NULL }
    };

    GLuint program = LoadShaders( shaders );
    glUseProgram( program );

    glVertexAttribPointer( vPosition, 2, GL_FLOAT,
                           GL_FALSE, 0, BUFFER_OFFSET(0) );
    glEnableVertexAttribArray( vPosition );
}

//----------------------------------------------------------------------------
//
// display
//

void
display( void )
{
    static const float black[] = { 0.0f, 0.0f, 0.0f, 0.0f };

    glClearBufferfv(GL_COLOR, 0, black);

    glBindVertexArray( VAOs[Triangles] );
    //非索引绘图
    //glDrawArrays( GL_TRIANGLES, 0, 3);
    
    //索引绘图 索引绑定到GL_ELEMENT_ARRAY_BUFFER
    //glDrawElements( GL_TRIANGLES, 3, GL_UNSIGNED_SHORT, NULL); 
    
    //索引点向后偏移3个元素，0,1,2时，绘制下一个三角形
    //glDrawElementsBaseVertex( GL_TRIANGLES, 3, GL_UNSIGNED_SHORT, NULL, 3);
    
    //实例化？
    glDrawArraysInstanced( GL_TRIANGLES, 0, 3, 2);
}

//----------------------------------------------------------------------------
//
// main
//

#ifdef _WIN32
int CALLBACK WinMain(
  _In_ HINSTANCE hInstance,
  _In_ HINSTANCE hPrevInstance,
  _In_ LPSTR     lpCmdLine,
  _In_ int       nCmdShow
)
#else
int
main( int argc, char** argv )
#endif
{
    glfwInit();

    GLFWwindow* window = glfwCreateWindow(800, 600, "Triangles", NULL, NULL);

    glfwMakeContextCurrent(window);
    glewInit();

    init();

    while (!glfwWindowShouldClose(window))
    {
        display();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
}
